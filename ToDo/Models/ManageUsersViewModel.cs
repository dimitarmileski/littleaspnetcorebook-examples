﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Services;

namespace ToDo.Models
{
    public class ManageUsersViewModel
    {
        public IEnumerable<ApplicationUser> Administrators
        {
            get;
            set;
        }
        public IEnumerable<ApplicationUser> Everyone { get; set; }
    }
}
