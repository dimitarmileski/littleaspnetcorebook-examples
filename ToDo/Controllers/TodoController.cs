﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDo.Models;
using ToDo.Services;

namespace AspNetCoreTodo.Controllers
{
    public class TodoController : Controller
    {
        private readonly ITodoItemService _todoItemService;

        public TodoController(ITodoItemService todoItemService)
        {
            _todoItemService = todoItemService;
        }

        [ActionName("Index")]
        public async Task<IActionResult> Index()
        {
            var todoItems = await _todoItemService.GetIncompleteItemsAsync();
            // Get to-do items from database
            // Put items into a model
            // Render view using the model
            var model = new TodoViewModel()
            {
                Items = todoItems
            };
            return View(model);
        }



        public async Task<IActionResult> AddItem(NewTodoItem newItem) {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var successful = await _todoItemService.AddItemAsync(newItem);
            if (!successful)
            {
                return BadRequest(new { error = "Could not add item" });
            }
            return Ok();
        }

        public async Task<IActionResult> MarkDone(Guid id)
        {
            if (id == Guid.Empty) return BadRequest();
            var successful = await _todoItemService.MarkDoneAsync(id);
            if (!successful) return BadRequest();
            return Ok();
        }
    }
}