﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Models;

namespace ToDo.Services
{
    public interface ITodoItemService
    {
        Task<IEnumerable<TodoItem>> GetIncompleteItemsAsync();

        Task<bool> AddItemAsync(NewTodoItem newItem);

        Task<bool> MarkDoneAsync(Guid id);
    }
}
